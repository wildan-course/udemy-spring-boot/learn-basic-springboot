package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.test.HelloWorldConfiguration;

public class ApplicationContextTest {

	
	@Test
	void testApplicationContext() {
		ApplicationContext context = new AnnotationConfigApplicationContext(HelloWorldConfiguration.class);

		Assertions.assertNotNull(context);
	}
}

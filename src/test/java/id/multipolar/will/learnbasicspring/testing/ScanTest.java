package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.data.Bar;
import id.multipolar.will.learnbasicspring.data.Foo;
import id.multipolar.will.learnbasicspring.test.ScanConfiguration;

public class ScanTest {

	private ConfigurableApplicationContext context;
	
	@BeforeEach
	void setup() {
		context = new AnnotationConfigApplicationContext(ScanConfiguration.class);
		context.registerShutdownHook();
	}
	
	@Test
	void testScan() {
//		Foo foo = context.getBean(Foo.class);
//		Bar bar = context.getBean(Bar.class);
		
	}
	
}

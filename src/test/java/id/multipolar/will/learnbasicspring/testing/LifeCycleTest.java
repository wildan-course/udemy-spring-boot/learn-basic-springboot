package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.data.Connection;
import id.multipolar.will.learnbasicspring.data.Server;
import id.multipolar.will.learnbasicspring.test.LifeCycleConfiguration;

public class LifeCycleTest {

	ConfigurableApplicationContext context;
	
	@BeforeEach
	void setUp() {
		context = new AnnotationConfigApplicationContext(LifeCycleConfiguration.class);
	}
	
	@AfterEach
	void tearDown( ) {
		context.close();
	}
	
	@Test
	void testConnection() {
		Connection conn = context.getBean(Connection.class);
	}
	
	@Test
	void testServer() {
		context.getBean(Server.class);
	}
	
}

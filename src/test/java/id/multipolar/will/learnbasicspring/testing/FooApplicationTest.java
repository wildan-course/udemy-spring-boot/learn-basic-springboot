package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.multipolar.will.learnbasicspring.application.FooApplication;
import id.multipolar.will.learnbasicspring.data.Foo;

@SpringBootTest(classes = FooApplication.class)
public class FooApplicationTest {

	@Autowired
	Foo foo;
	
	@Test
	void testSpringBoot() {
		Assertions.assertNotNull(foo);
	}
	
}

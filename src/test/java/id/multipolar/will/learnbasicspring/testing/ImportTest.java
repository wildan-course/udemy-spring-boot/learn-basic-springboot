package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.data.Bar;
import id.multipolar.will.learnbasicspring.data.Foo;
import id.multipolar.will.learnbasicspring.test.MainConfiguration;

public class ImportTest {

	private ConfigurableApplicationContext context;
	
	@BeforeEach
	void setUp() {
		context = new AnnotationConfigApplicationContext(MainConfiguration.class);
		context.registerShutdownHook();
	}
	
	@Test
	void testImport() {
		Foo foo = context.getBean(Foo.class);
		Bar bar = context.getBean(Bar.class);
	}
	
	
	
}

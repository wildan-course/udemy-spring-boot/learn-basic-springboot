package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.data.Foo;
import id.multipolar.will.learnbasicspring.test.DependsOnConfiguration;

public class DependsOnTest {

	ApplicationContext context;
	
	@BeforeEach
	void setUp() {
		context = new AnnotationConfigApplicationContext(DependsOnConfiguration.class);
	}
	
	@Test
	void testDependsOn() {
		Foo foo = context.getBean(Foo.class);
	}
}

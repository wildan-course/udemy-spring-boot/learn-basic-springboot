package id.multipolar.will.learnbasicspring.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import id.multipolar.will.learnbasicspring.factory.PaymentGatewatClientFactoryBean;

@Configuration
@Import({
	PaymentGatewatClientFactoryBean.class
})
public class FactoryConfiguration {

}

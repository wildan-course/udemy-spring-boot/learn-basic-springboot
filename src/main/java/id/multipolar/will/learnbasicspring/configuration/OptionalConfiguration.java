package id.multipolar.will.learnbasicspring.configuration;

import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.multipolar.will.learnbasicspring.data.Bar;
import id.multipolar.will.learnbasicspring.data.Foo;
import id.multipolar.will.learnbasicspring.data.FooBar;

@Configuration
public class OptionalConfiguration {

	@Bean
	public Foo foo() {
		return new Foo();
	}
	
	@Bean
	public FooBar fooBar(Optional<Foo> foo, Optional<Bar> bar) {
		return new FooBar(foo.orElse(null), bar.orElse(null));
	}
	
}

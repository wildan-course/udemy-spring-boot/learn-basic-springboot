package id.multipolar.will.learnbasicspring.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import id.multipolar.will.learnbasicspring.service.MerchantServiceImpl;

@Configuration
@Import({
	MerchantServiceImpl.class
})
public class InheritanceConfiguration {

	
	
}

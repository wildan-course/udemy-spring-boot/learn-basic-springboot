package id.multipolar.will.learnbasicspring.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;

import id.multipolar.will.learnbasicspring.data.User;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LoginSuccessEvent extends ApplicationEvent{

	@Getter
	private final User user;
	
	public LoginSuccessEvent(User user) {
		super(user);
		this.user = user;
	}
	
}

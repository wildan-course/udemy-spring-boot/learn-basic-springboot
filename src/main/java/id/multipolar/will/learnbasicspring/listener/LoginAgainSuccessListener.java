package id.multipolar.will.learnbasicspring.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import id.multipolar.will.learnbasicspring.event.LoginSuccessEvent;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class LoginAgainSuccessListener implements ApplicationListener<LoginSuccessEvent>{@Override
	public void onApplicationEvent(LoginSuccessEvent event) {
		log.info("Success again login for user {}", event.getUser());
	}

}

package id.multipolar.will.learnbasicspring.data;

import org.springframework.stereotype.Component;

import id.multipolar.will.learnbasicspring.aware.IdAware;
import lombok.Getter;

@Component
public class Car implements IdAware{

	@Getter
	private String id;

	@Override
	public void setId(String id) {
		this.id = id;
	}
	
}

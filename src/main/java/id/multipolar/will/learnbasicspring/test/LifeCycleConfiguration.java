package id.multipolar.will.learnbasicspring.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.multipolar.will.learnbasicspring.data.Connection;
import id.multipolar.will.learnbasicspring.data.Server;

@Configuration
public class LifeCycleConfiguration {

	@Bean
	public Connection connection() {
		return new Connection();
	}
	
	
//	@Bean(initMethod = "start", destroyMethod="stop")
	@Bean
	public Server server() {
		return new Server();
	}
}

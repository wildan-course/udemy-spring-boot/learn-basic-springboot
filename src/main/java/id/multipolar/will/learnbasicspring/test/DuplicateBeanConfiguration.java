package id.multipolar.will.learnbasicspring.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.multipolar.will.learnbasicspring.data.Foo;

@Configuration
public class DuplicateBeanConfiguration {

	
	@Bean
	public Foo foo1() {
		return new Foo();
	}
	
	@Bean
	public Foo foo2() {
		return new Foo();
	}
}

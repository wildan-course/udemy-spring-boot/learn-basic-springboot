package id.multipolar.will.learnbasicspring.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.multipolar.will.learnbasicspring.data.cyclic.CyclicA;
import id.multipolar.will.learnbasicspring.data.cyclic.CyclicB;
import id.multipolar.will.learnbasicspring.data.cyclic.CyclicC;

@Configuration
public class CyclicConfiguration {

	@Bean
	public CyclicA cyclicA(CyclicB cyclicB) {
		return new CyclicA(cyclicB);
	}
	
	@Bean
	public CyclicB cyclicB(CyclicC cyclicC) {
		return new CyclicB(cyclicC);
	}
	
	@Bean
	public CyclicC cyclicC(CyclicA cyclicA) {
		return new CyclicC(cyclicA);
	}
	
}

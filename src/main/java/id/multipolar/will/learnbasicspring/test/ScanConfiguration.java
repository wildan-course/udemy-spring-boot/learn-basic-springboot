package id.multipolar.will.learnbasicspring.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
		"id.multipolar.will.learnbasicspring.configuration"
})
public class ScanConfiguration {

}
